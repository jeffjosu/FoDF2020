# - Circuits | Embedded programming

In this part I put into practice Embedded programming - Inputs, Outputs & Communication.

I divided it into 3 parts in order to make easier a replication of each aspect of the project.

## Wheeled robot | Output devices

### Softwares and hardwares

I programmed an [Arduino UNO](https://www.arduino.cc/en/Guide/ArduinoUno) board in C++ with the [Arduino IDE](https://www.arduino.cc/en/software).  

**Components:**

| Quantity | Description                                  |
| -------- | -------------------------------------------- |
| 1 x      | [Arduino UNO board](../images/FinalProject/UnoBoard.jpg) |
| 1 x      | [Adafruit Motor Shield V2.0](../images/FinalProject/MotorShield.jpg) |
| 3 x      | 5V DC Motor        |
| 2 x      | [UR18650 Li-Ion Battery 2600mAh 3.7V](../images/FinalProject/Battery.jpg) |
| 2 x      | [Battery holder with mini-USB charging module](../images/FinalProject/BatteryHolder.jpg) |
| 1 x      | ON/OFF switch                               |
| a few    | Rigid cables                                |

### Circuit

I connected the motors to the Motor Shield which is already tagged with the motor connections:

**Physical circuit:**

  ![RobotCircuit](../images/FinalProject/RobotCircuit.jpg)

### Coding

- First of all, I installed the _Adafruit Motor Shield V2 Library_ into the Arduino IDE.

- Add libraries for Motor Shield and Math functions: 
```C
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <math.h> 
```

- Create Motor shield object: 
```C
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
```

- Declare motors M1, M2 and M3 on their respective ports:
```C
Adafruit_DCMotor *Motor1 = AFMS.getMotor(1);
Adafruit_DCMotor *Motor2 = AFMS.getMotor(2);
Adafruit_DCMotor *Motor3 = AFMS.getMotor(3);
```

- Declare angle in rads of each wheel with respect to Y-axis of the robot (correspond to 0º, 120º and 240º), this part is used a bit later:
```C
const double AngWheels[3] = {0, 2.094, 4.189}; 
```

- In the setup we initialize the Motor Shield ports previously declared:
```C
AFMS.begin(); //Initialize Motor shield ports
```

- The loop:  
    - First, there are two main commands for the Motor shield:   
        - `_MotorName_->run(_direction of rotation_);` where _MotorName_ is the name we used to declare them, and _direction of rotation_ can be FORWARD or BACKWARD, it depends on the desired rotation or calculated by the signal of the _speed_.  
        - `_MotorName_->setSpeed(_speed_);` where _speed_ needs is the desired/calculated one from 0 to 255, by default it takes it as absolute values. To stop the motor, we can use `_MotorName_->run(RELEASE)` or `_MotorName_->setSpeed(_speed_)`.  
    - Logic explanation:  
        - If there's a radio signal, the code starts.  
        - Read values from the Transmitter.  
        - There are two possibilities: read Joystick_1 or Joystick_2:  
            - If Joystick_2 is pushed, check what position it is:  
                - Right: make a Clockwise rotation.  
                - Left: make a Counter-Clockwise rotation.  
            - If Joystick_1 is pushed, check what position it is:  
                - Up: Move forward.  
                - Down: Move backward.  
                - Right: Move to the right.  
                - Left: Move to the left.  
                - For all of the possible positions, we got to calculate the speed for each wheel by using the holonomic drive function.  
            - If we release them or do not push them, then stop the motors.

```C
int Values[3];
if (radio.available()) {
  //[here I receive the values from the transmitter]
      
    if (Values[2]<=-5 || Values[2]>=5) //Detect if the Joystick_2 is activated
    { 
      int speed = map(Values[2],-100,100,-60,60); //Read the percentage values and convert them to speed values for the motors
      
      //Give direction of rotation to each wheel, depending on direction we push the Joystick
      if (Values[2]<-4) 
      { Motor1->run(FORWARD);
        Motor2->run(FORWARD);
        Motor3->run(FORWARD);}
      if (Values[2]>4)
      { Motor1->run(BACKWARD);
        Motor2->run(BACKWARD);
        Motor3->run(BACKWARD);}
        
      //Write speed in each motor
      Motor1->setSpeed(speed);
      Motor2->setSpeed(speed);
      Motor3->setSpeed(speed);
      
      } else if ((Values[0]<=-4 || Values[0]>=4) || (Values[1]<=-4 || Values[1]>=4))
      {
          //Calculate angle of movement of robot (and Read the percentage values and convert them to speed values for the motors)
          double Vx=map(Values[1],-100,100,-230,230);
          double Vy=map(Values[0],-100,100,-230,230);
          double theta = atan2(Vx, Vy);

          //Calculate speed for every wheel, using the formula holonomic drive for 3W omnidirectional robots:
          double SpeedM1 = ((cos(theta)*cos(AngWheels[0]) - sin(theta)*sin(theta + AngWheels[0]))*Vx) - ((sin(theta)*cos(theta + AngWheels[0]) + cos(theta)*sin(theta + AngWheels[0]))*Vy);
          double SpeedM2 = ((cos(theta)*cos(AngWheels[1]) - sin(theta)*sin(theta + AngWheels[1]))*Vx) - ((sin(theta)*cos(theta + AngWheels[1]) + cos(theta)*sin(theta + AngWheels[1]))*Vy);
          double SpeedM3 = ((cos(theta)*cos(AngWheels[2]) - sin(theta)*sin(theta + AngWheels[2]))*Vx) - ((sin(theta)*cos(theta + AngWheels[2]) + cos(theta)*sin(theta + AngWheels[2]))*Vy);

          //Give direction of rotation to each wheel, depending on the signal of their speed
          if (SpeedM1<0){ Motor1->run(BACKWARD);}
          else if (SpeedM1>0){ Motor1->run(FORWARD);}
          if (SpeedM2<0){ Motor2->run(BACKWARD);}
          else if (SpeedM2>0){ Motor2->run(FORWARD);}
          if (SpeedM3<0){ Motor3->run(BACKWARD);}
          else if (SpeedM3>0){ Motor3->run(FORWARD);}

          //Write speed in each motor
          Motor1->setSpeed(SpeedM1);
          Motor2->setSpeed(SpeedM2);
          Motor3->setSpeed(SpeedM3);
      } else { //Deactivate motors
        Motor1->setSpeed(0);
        Motor2->setSpeed(0);
        Motor3->setSpeed(0);}     
  }
```

**Full code:**

```C
//Libraries for Communication part (nRF24)
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); //Create radio object with and CE & CSN pins
const byte address[6] = "00111"; //Receiver Communication address

//Libraries for Motor shield
#include <Wire.h>
#include <Adafruit_MotorShield.h>

//Create Motor shield object with the default I2C address 
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

//Declare motors (M1, M2 and M3) on their respective ports.
Adafruit_DCMotor *Motor1 = AFMS.getMotor(1);
Adafruit_DCMotor *Motor2 = AFMS.getMotor(2);
Adafruit_DCMotor *Motor3 = AFMS.getMotor(3);

const double AngWheels[3] = {0, 2.094, 4.189}; //Angle in rads of each wheel with respect to Y-axis of the robot (correspond to 0º, 120º and 240º)

//Library for math functions
#include <math.h> 

void setup() {
  Serial.begin(9600); //Begin Serial communication at 9600 bauds

  radio.begin(); //Initialize radio object
  radio.setPALevel(RF24_PA_HIGH); //Set radio range
  radio.openReadingPipe(0, address); //Set address to transmitter
  radio.startListening(); //Set module as receiver

  AFMS.begin(); //Initialize Motor shield ports
}

void loop() {
  int Values[3];
  
  if (radio.available()) {  
    radio.read(&Values, sizeof(Values)); //Read array of Values from Transmitter
        
      if (Values[2]<=-5 || Values[2]>=5) //Detect if the Joystick_2 is activated
      { int speed = map(Values[2],-100,100,-60,60); //Read the percentage values and convert them to speed values for the motors
        
        //Give direction of rotation to each wheel, depending on direction we push the Joystick
        if (Values[2]<-4) 
        { Motor1->run(FORWARD);
          Motor2->run(FORWARD);
          Motor3->run(FORWARD);
          Serial.println("CCW rotation");}
        if (Values[2]>4)
        { Motor1->run(BACKWARD);
          Motor2->run(BACKWARD);
          Motor3->run(BACKWARD);
          Serial.println("CC rotation");}
          
        //Write speed in each motor
        Motor1->setSpeed(speed);
        Motor2->setSpeed(speed);
        Motor3->setSpeed(speed);
        } else if ((Values[0]<=-4 || Values[0]>=4) || (Values[1]<=-4 || Values[1]>=4))
        {
            //Calculate angle of movement of robot (and Read the percentaje values and convert them to speed values for the motors)
            double Vx=map(Values[1],-100,100,-230,230);
            double Vy=map(Values[0],-100,100,-230,230);
            double theta = atan2(Vx, Vy);

            //Calculate speed for every wheel, using the formula holonomic drive for 3W omnidirectional robots:
            double SpeedM1 = ((cos(theta)*cos(AngWheels[0]) - sin(theta)*sin(theta + AngWheels[0]))*Vx) - ((sin(theta)*cos(theta + AngWheels[0]) + cos(theta)*sin(theta + AngWheels[0]))*Vy);
            double SpeedM2 = ((cos(theta)*cos(AngWheels[1]) - sin(theta)*sin(theta + AngWheels[1]))*Vx) - ((sin(theta)*cos(theta + AngWheels[1]) + cos(theta)*sin(theta + AngWheels[1]))*Vy);
            double SpeedM3 = ((cos(theta)*cos(AngWheels[2]) - sin(theta)*sin(theta + AngWheels[2]))*Vx) - ((sin(theta)*cos(theta + AngWheels[2]) + cos(theta)*sin(theta + AngWheels[2]))*Vy);

            //Give direction of rotation to each wheel, depending on the signal of their speed
            if (SpeedM1<0){ Motor1->run(BACKWARD);}
            else if (SpeedM1>0){ Motor1->run(FORWARD);}
            if (SpeedM2<0){ Motor2->run(BACKWARD);}
            else if (SpeedM2>0){ Motor2->run(FORWARD);}
            if (SpeedM3<0){ Motor3->run(BACKWARD);}
            else if (SpeedM3>0){ Motor3->run(FORWARD);}

            //Write speed in each motor
            Motor1->setSpeed(SpeedM1);
            Motor2->setSpeed(SpeedM2);
            Motor3->setSpeed(SpeedM3);

            if (Values[1]<-4) {Serial.println("Moving to the Left");}
            if (Values[1]>4) {Serial.println("Moving to the Right");}
            if (Values[0]<-4) {Serial.println("Moving Backward");}
            if (Values[0]>4) {Serial.println("Moving Forward");}
            Serial.print("M1: ");
            Serial.println(SpeedM1);
            Serial.print("M2: ");
            Serial.println(SpeedM2);
            Serial.print("M3: ");
            Serial.println(SpeedM3);
            Serial.println("");
        } else { //Deactivate motors
          Motor1->setSpeed(0);
          Motor2->setSpeed(0);
          Motor3->setSpeed(0);}     
   }
}
```

### Download files

\- ODRobot Code: [ReceiverOfficial.ino](../images/FinalProject/ReceiverOfficial/ReceiverOfficial.ino) 

----------------------------------------------------

## Remote control | Input devices

I used two joysticks to control horizontal movements (forward, backward, left and right) and rotations (Clockwise and Counter-Clockwise).  

### Softwares and hardwares

I programmed a [Joy-IT Nano-V3](https://joy-it.net/de/products/ard_NanoV3) board (CH340G USB-TTL converter) in C++ with the [Arduino IDE](https://www.arduino.cc/en/software).  

**Components:**

| Quantity | Description                                     |
| -------- | ----------------------------------------------- |
| 1 x      | [Joy-IT Nano-V3 board](../images/FinalProject/JOYIT.jpg) |
| 2 x      | [Grove - Thumb Joystick v1.1](../images/FinalProject/Joystick.jpg)  |
| 1 x      | [UR18650 Li-Ion Battery 2600mAh 3.7V](../images/FinalProject/Battery.jpg) | 
| 1 x      | [Battery holder with mini-USB charging module](../images/FinalProject/BatteryHolder.jpg) |
| 1 x      | Prototyping PCB |
| 1 x      | ON/OFF switch                               |
| a few    | Rigid cables                                |

### Circuit

**Pin configuration:**

Joystick 1 (for the horizontal movements):

| Pin | Connection    |
| --- | -----------   |
| Vcc | 5V            |
| GND | GND           |
| x   | Joy-IT Pin A0 |
| y   | Joy-IT Pin A1 |

Joystick 2 (for the rotations):

| Pin | Connection    |
| --- | -----------   |
| Vcc | 5V            |
| GND | GND           |
| x   | Joy-IT Pin A2 |
| y   | Joy-IT Pin A3 |

>Joystick 2 x-pin was connected but not used, then the remote control could be re-used in another project such as a basic drone.

**Physical circuit:**

  ![RemoteControlCircuit](../images/FinalProject/RemoteControlCircuit.jpg)

### Coding

- First of all, we got to download and install the _CH340G driver_ due to it's not in the IDE Arduino by default; without it the computer couldn't recognize the board. I founded it [here](http://www.wch-ic.com/downloads/CH341SER_ZIP.html).  

- Get the Joysticks readings:
```C
int Values[3]; //

//Read values from Joysticks and convert them to percentage
Values[0] = map(analogRead(A0),0,1023,100,-100); //Forward and Backward movement from Joystick_1
Values[1] = map(analogRead(A1),0,1023,-100,100); //Left and Right movement from Joystick_1
Values[2] = map(analogRead(A3),0,1023,-100,100); //Rotation movement from Joystick_2

//[here I sent the Values to the receiver]
```

**Full code:**

```C
//Libraries for Communication part (nRF24)
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); //Create radio object with and CE & CSN pins
const byte address[6] = "00111"; //Transmitter communication address

void setup() {
  radio.begin(); //Initialize radio object
  radio.setPALevel(RF24_PA_HIGH); //Set radio range
  radio.openWritingPipe(address); //Set address to receiver
  radio.stopListening(); //Set module as transmitter
}

void loop() {
  int Values[3]; //

  //Read values from Joysticks and convert them to percentage
  Values[0] = map(analogRead(A0),0,1023,100,-100); //Forward and Backward movement from Joystick_1
  Values[1] = map(analogRead(A1),0,1023,-100,100); //Left and Right movement from Joystick_1
  Values[2] = map(analogRead(A3),0,1023,-100,100); //Rotation movement from Joystick_2

  radio.write(&Values, sizeof(Values)); //Send to Receiver an array that containing the Values

  delay(100); //Send values every 0.1 seconds
}
```

### Download files

\- Remote control Code: [TransmitterOfficial.ino](../images/FinalProject/TransmitterOfficial/TransmitterOfficial.ino) 

----------------------------------------------------

## Communication

This is the explanation of the communication part, which is already contained on the codes of Wheeled (which I will refer to as "Receiver") robot and Remote control (which I will refer to as "Transmitter").

> I learned how to do it with the [nRF24L01 tutorial of _How to Mechatronics_](https://howtomechatronics.com/tutorials/arduino/arduino-wireless-communication-nrf24l01-tutorial/).

**Components:** 2 x [nRF24L01](../images/FinalProject/RFModule.jpg) wireless transceiver module, jumper cables

### Pin configuration

Pins 11, 12 and 13 are the SPI connections on both of the boards.

\- Transmitter:

| Pin  | Connection    |
| ---- | ------------- |
| Vcc  | 3.3V          |
| CE   | Joy-IT Pin 7  |
| CSN  | Joy-IT Pin 8  |
| SCK  | Joy-IT Pin 13 |
| MOSI | Joy-IT Pin 11 |
| MISO | Joy-IT Pin 12 |
| GND  | GND           |

\- Receiver:

| Pin  | Connection     |
| ---- | -------------- |
| Vcc  | 3.3V           |
| CE   | Arduino Pin 7  |
| CSN  | Arduino Pin 8  |
| SCK  | Arduino Pin 13 |
| MOSI | Arduino Pin 11 |
| MISO | Arduino Pin 12 |
| GND  | GND            |

### Coding

- First of all we got to install the _RF24 (by TMRh20)_. 

- Import the SPI and RF24 libraries:
```C
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
```

- Create RF24 object and specify the pins for CSN and CE connection:
```C
RF24 radio(7, 8);
```

- Create an address for the communication:
```C
const byte address[6] = "00111";
```

- In the setup we initialize radio object and set the PowerAmplifier level:
```C
radio.begin();
radio.setPALevel(RF24_PA_MIN);
```

- In the setup of the Transmitter, we set address to receiver, and set the module as transmitter:
```C
radio.openWritingPipe(address);
radio.stopListening();
```

- In the setup of the Receiver, we set address to transmitter, and set the module as receiver:
```C
radio.openReadingPipe(0, address);
radio.startListening();
```

- In the loop of the Transmitter, we just need to send the message:
```C
//[here I declared "int Values[3];" and got the reads from the joysticks]
radio.write(&Values, sizeof(Values));
```

- In the loop of the Receiver, we just need to read the message if there is a radio connection:
```C
//[here I declared "int Values[3];"]
if (radio.available()) {
radio.read(&Values, sizeof(Values));
}
```

### Download files

\- nRF24L01 Transceiver Datasheet: [nRF24L01_Datasheet.pdf](../images/FinalProject/nRF24L01_Datasheet.pdf)  
\- nRF24L01 Transmitter example code: [RF_Transmitter.ino](../images/FinalProject/RF_Transmitter/RF_Transmitter.ino)  
\- nRF24L01 Receiver example code: [RF_Receiver.ino](../images/FinalProject/RF_Receiver/RF_Receiver.ino) 
