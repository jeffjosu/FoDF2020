# Omnidirectional robot

For the final project we should include aspects from all or most of the lectures taken during this course.

## Getting the idea

First of all, I brainstormed for inspiration by looking for final projects from other students from previous years and watching electronics project videos on YouTube, these were the possible general ideas to follow:

\- Coin Sorting Machine.  
\- Word clock.  
\- Assistant for blind people.  
\- Wheeled robot.

Because of the diverse applicability, utility and possible future improvements, and ca I chose to make a Three-wheel Omnidirectional robot.

**Concept:**

- This project is done for learning purposes.  

- Since the concept of this course is, from my point of view, more focus to be a first approach to digital fabrication than an advanced course, this project was done in a way that can be easily understood by other students.  
  
## Making

This project can be divided into two main parts which are the Omnidirectional robot and a Remote control:

### Robot

#### Frame

  ![CutFrame](../images/FinalProject/CutFrame.jpg)  

- I designed it in Fusion 360.  
- I cut it in Plexiglas with an Epilog Zing 30 Watts.

>Find [here](https://jeffjosu.gitlab.io/JeffPage/Project/ww02-Frame/) the making process details of both the Robot and the Remote control frames.

#### Wheels

  ![PrintedOmni3](../images/FinalProject/PrintedOmni3.jpg)

There are a lot of omniwheel sets available on the internet, but for this course we should proof our own manufacturing skills (learned during the course), then I made mine ones.

- I designed it in Fusion 360.  
- I printed them wheels in generic PLA with an Ultimaker cura using .  
- For the assembly part I used 1-inch nails as a shaft for the rollers.

>Find [here](https://jeffjosu.gitlab.io/JeffPage/Project/ww03-Wheels/) the making process details of the wheels.  

#### Circuit

  ![RobotCircuit](../images/FinalProject/RobotCircuit.jpg)  

  ![Robot3](../images/FinalProject/Robot3.jpg)

I used:  
- Arduino UNO board.  
- Adafruit Motor shield.   
- 3 5V-DC motors.  
- nRF24 transceiver for communication with the Remote control.  
- 2 UR18650 Li-Ion Battery 2600mAh 3.7V connected in series.  
- ON/OFF switch.

>Find [here](https://jeffjosu.gitlab.io/JeffPage/Project/ww04-ControlPart/#wheeled-robot-output-devices) the making process details of the circuit and codes.

### Remote control

#### Frame

  ![CutRemoteControl](../images/FinalProject/CutRemoteControl.jpg) 

- I designed it in Fusion 360.  
- I cut it in Plexiglas with an Epilog Zing 30 Watts.

>Find [here](https://jeffjosu.gitlab.io/JeffPage/Project/ww02-Frame/) the making process details of both the Remote control and the Robot frames.

#### Circuit

  ![Control1](../images/FinalProject/Control1.jpg)  

I used:  
- Joy-IT Nano-V3 board.  
- 2 Joysticks to control:   
    - Horizontal movements (forward, backward, left and right).  
    - Rotations (Clockwise and Counter-Clockwise).   
- nRF24 transceiver for the communication with the Robot.
- UR18650 Li-Ion Battery 2600mAh 3.7V.  
- ON/OFF switch.

>Find [here](https://jeffjosu.gitlab.io/JeffPage/Project/ww04-ControlPart/#remote-control-input-devices) the making process details of the circuits and codes.

## Demonstration

Robot:
  ![Robot1](../images/FinalProject/Robot1.jpg)  

  ![Robot2](../images/FinalProject/Robot2.jpg)

Remote control:
  ![Control2](../images/FinalProject/Control2.jpg) 

**Video:** 

<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
  <iframe src="https://www.youtube.com/embed/w9XUuxVxQsE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
</div>

## Final project Outlook

A few things can be improved in my Final project, which is basically to do it for a real application rather than a learning purpose:

- Use gearbox with the motors to get all the torque it needs to perform much smoother and slower movements; this part is probably the most important improvement since at the end of the project the robot wasn't really able to move sideways and moved a little wavy when moving forward and backwards, but had no problems with rotations.  
- Code can be done in a way to be able to rotate at the same time it's moving horizontally, this will be possible also for the application of the gearboxes.  
  
- Add encoders to the motor for getting a feedback and apply PID controller, this way get much straighter movements.  
- Change the 1-inch nails for proper rounded shaft, due to the nails have a kind of flatted shape just before the head and it annoys the movement.  
- Add bearings would improve a bit more the performance of the rolling.  

Since I'm going to attend the FabAcademy 2021, one possible Final project could be based on taking these points in consideration.

## Course experience

It seems to be a really small and simple project (and it is), but there's a lot of things to take care of during the whole process: parameters, materials, measurements, basic programming logic and electronics, and the most important... patience and dedication for the documentation.

This course meant for my an improvement of my manufacturing skills with new theory and a very practical experience.

The whole process of making all of the assignments couldn't have being possible without valuable aid of of our instructor, other FabLab workers, and also my colleagues from the IoT-lab.