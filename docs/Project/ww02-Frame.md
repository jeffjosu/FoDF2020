# - Frames | 2D design & Laser cut

I designed and cut the frame for the Robot and the Remote control.

## Softwares and hardwares

\- For designing I used [Fusion 360](https://www.autodesk.com/education/edu-software/overview?sorting=featured&page=1#).  
\- For setting up the laser cutter I used [RhinoCeros 5.0](https://www.rhino3d.com/).  
\- For cutting I used the Epilog Zing 30 Watts Laser cutter.  
\- The material I cut was 3.5mm Plexiglas.

## Design

### Kerf

For the Kerf measurement I used the same technique I applied in the [Parametric design & Laser cutting](https://jeffjosu.gitlab.io/JeffPage/assignments/week03/#kerf) assignment, getting this results:

Amount of pieces: N = 3  
Designed total length: DTL = 3x15 = 45mm  
Real total length: RTL = 44.35mm  
Kerf = (DTL-RTL) / N = (45-44.35)mm/3 = 0.65mm/3 ≈ 0.22mm

### Parametric design

- Taking in consideration the Kerfs and material thickness as parameters, I designed the frames:  
  ![DesignParameters](../images/FinalProject/DesignParameters.jpg)  
  ![FrameSketch1](../images/FinalProject/FrameSketch1.jpg)  
  ![FrameSketch3](../images/FinalProject/FrameSketch3.jpg)  
  ![FrameSketch2](../images/FinalProject/FrameSketch2.jpg)

## Laser cut

### Laser parameters

For the cut I used these parameters:  
  ![Parameters](../images/FinalProject/CutParameters.jpg)

> Further explanation about parameters, check [Laser cutter Parameters](https://jeffjosu.gitlab.io/JeffPage/assignments/week03/#processes-parameters).

### Laser cutting

- Designs in the RhinoCeros:  
  ![Rhino1](../images/FinalProject/Rhino1.jpg)  
  ![Rhino2](../images/FinalProject/Rhino2.jpg)  

- Cut frames:  
  ![CutFrame](../images/FinalProject/CutFrame.jpg)  
  ![CutRemoteControl](../images/FinalProject/CutRemoteControl.jpg)  

> Further explanation about the cutting process, check [Laser cutter Process](https://jeffjosu.gitlab.io/JeffPage/assignments/week03/#steps).

## Download files

\- Frames: [TestPiece.f3d](../images/FinalProject/Frames.f3d) 