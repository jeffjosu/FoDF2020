# About me

![](../images/About.jpg)

Hi! I'm Jefferson Josue Sandoval Betanco, I was born in 1998 in a warm city called León, Nicaragua. I arrived in Germany on October 2019 for an exchange semester at the Hochschule Rhein Waal and I'm actually taking another semester here.

I enjoy the process of bringing an idea to reality, supported by the method of learning by doing, which I consider is the best way to get new and practical knowledge.

## Studies and experience

- Mechatronics engineering finalist student at Universidad Tecnológica La Salle, León, Nicaragua. <br> 2015 - present

- Academic exchange semesters at the Hochschule Rhein Waal Kamp-Lintfort, Germany <br> ISAP-DAAD program | WS 2019/20 & WS 2020/21 (present)

- Internship at the Hochschule Rhein Waal, working on GreenFabLab renewable energy projects and drone experiments as part of the SPECTORS cooperation project. <br> IAESTE-DAAD program | Mar 2020 - Oct 2020
