# 2D and 3D Design | Hook

This second week I worked on getting used to 2D and 3D design.

For this assignment I searched for some examples of technical drawing with measurements, and I found this hook:  
![Hook](../images/week02/Hook.png)  
You can find the picture [here](https://autocadparatodos.blogspot.com/2019/)

## Softwares

\- For the 2D designing we used [LibreCAD](https://librecad.org/#download).  
\- For 3D designing we used [Fusion 360](https://www.autodesk.com/education/edu-software/overview?sorting=featured&page=1#).

## 2D design

This was my first approach to LibreCAD, but this is just about getting used to the interface.

- Every were done using the basic figures in the menu on the left.  
  The ones I used for this drawing were _Vertical line_, _Horizontal line_, [2 points, Radius] _Circle_,[Center, Radius] _Circle_ and _Spline_:
  ![2D_Hook](../images/week02/2D_Design_Hook5.jpg)

- Once it was done, I used the tool _Trim_ for deleting the lines I don't want:
  ![2D_Hook](../images/week02/2D_Design_Hook4.jpg)

- This is the result:
  ![2D_Hook](../images/week02/2D_Design_Hook.jpg)

- To make sure that all of the dimensions are right, I used the _Narrow_ tool, which allow us to measure vertical and horizontal lines, radius and diameters:
  ![2D_Hook](../images/week02/2D_Design_Hook2.jpg)
  ![2D_Hook](../images/week02/2D_Design_Hook3.jpg)

## 3D design

I already had some experience on basic 3D design with Fusion 360, and this time there were two new commands for me which I had to use a lot: _Loft_ and _Plane at angle_.

- I used the 2D file I made in LibreCAD, so first step was to import it into Fusion.

- I started with the"main" part of the hook; since this is not a specific geometric figure and also has different diameters, I can't use the very basic 3D commands (_Extrude_, _Revolve_, _Box_, _Cylinder_ or _Sphere_), so I used _Loft_:

      - Divided the shape into various parts and then _Finish Sketch_:
      ![Sketch](../images/week02/3D_Design_Hook01.jpg)
      > Note: Every part should have have a smooth and simple shape.
      - Create planes in every line: In _Solid_ tab > _Construct_ > _Plane at angle_ > Select one line > Type 90deg > _OK_:
      ![Sketch](../images/week02/3D_Design_Hook02.jpg)
      - Make circles on all of the created planes:
      ![Circles](../images/week02/3D_Design_Hook03.jpg)
      - Create the _loft_: In _Solid_ tab > _Create_ > _Loft_ > Select two continuous _Profiles_ > Add the _Rails_ like shown:
      ![Loft](../images/week02/3D_Design_Hook04.jpg)

- I made the above parts with sketches and extrusion:
  ![Loft](../images/week02/3D_Design_Hook05.jpg)
  ![3D_Hook](../images/week02/3D_Design_Hook.jpg)

A full view of the model:

<div style="position: relative; padding-bottom: 65%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
    <iframe title="A 3D model" width="640" height="480" src="https://sketchfab.com/models/2d317138bb5046d685969eb12d5536ba/embed?autospin=0.2&amp;autostart=1&amp;ui_controls=1&amp;ui_infos=1&amp;ui_inspector=1&amp;ui_stop=1&amp;ui_watermark=1&amp;ui_watermark_link=1" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
</div>
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/3d-models/3d-hook-2d317138bb5046d685969eb12d5536ba?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">3D_Hook</a>
    by <a href="https://sketchfab.com/jeffjosu?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Jeff Josue</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>

## Downloads

\- 2D Design - Hook: [2D_Hook.dxf](../images/week02/2D_Design_Hook.dxf)

\- 3D design - Hook: [3D_Hook.f3d](../images/week02/3D_Design_Hook.f3d)
