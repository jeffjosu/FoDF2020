# Output devices | Safety area

This seventh week I worked on getting used to output devices.

For this assignment we had to develop a program that uses an output device.

I made a program that simulates limits in a danger zone, displaying the distance (taken from an ultrasonic sensor) with a message on the LCD and activating a RGB LED according to the distance:

Logic:

\-Program starts:  
LED = solid blue  
LCD message = “Fundamentals2020 FabLab - KaLi”

_message scrolls to the left; the reading of the distance starts_

\*More than 15cms (safe zone):  
LED = solid green  
LCD message = “Distance: --cm Safe area"

\*Between 10 and 15cms (caution zone):  
LED turns yellow  
LCD message = “Distance: --cm Limit area"

\*Less than 10cms (danger zone):  
LED blinks in red  
LCD message = “Distance: --cm Danger!"

## Software and hardwares

\- For this practice we programmed an [Arduino UNO](https://www.arduino.cc/en/Guide/ArduinoUno) with its [Arduino IDE](https://www.arduino.cc/en/software).

**Components:**

For this assignment I used:

| Quantity | Description                        |
| -------- | ---------------------------------- |
| 1 x      | Arduino UNO                        |
| 1 x      | HC-SR04 Ultrasonic distance sensor |
| 1 x      | LCM-S01602DTR/M LCD 16x2           |
| 1 x      | B10K Potentiometer                 |
| 1 x      | RGB LED                            |
| 3 x      | Resistor 150 Ohms                  |
| 2 x      | Small breadboard                   |
| a lot    | Jumper cables                      |

## Circuit

**Pin configuration:**

\- HC-SR04 Ultrasonic distance sensor:

| Pin     | Connection    |
| ------- | ------------- |
| Vcc     | 5V            |
| GND     | GND           |
| Echo    | Arduino Pin 6 |
| Trigger | Arduino Pin 7 |

  ![Circuit](../images/week07/Ultrasonic.jpg)

\- LCM-S01602DTR/M LCD 16x2:

| Pin no. | Pin name                    | Connection           |
| ------- | --------------------------- | -------------------- |
| 1       | Vss (ground)                | GND                  |
| 2       | Vdd (+5V)                   | 5V                   |
| 3       | V0 (Contrast V)             | Potentiometer Output |
| 4       | RS (Register select signal) | GND                  |
| 5       | R/W (Read/Write)            | Arduino Pin 13       |
| 6       | E (Enable)                  | Arduino Pin 12       |
| 11      | DB4 (Data Bus 4)            | Arduino Pin 5        |
| 12      | DB5 (Data Bus 5)            | Arduino Pin 4        |
| 13      | DB6 (Data Bus 6)            | Arduino Pin 3        |
| 14      | DB7 (Data Bus 7)            | Arduino Pin 2        |

  ![Circuit](../images/week07/LCD1.jpg)
  ![Circuit](../images/week07/LCD2.jpg)

\- B10K Potentiometer:

| Pin    | Connection     |
| ------ | -------------- |
| Vcc    | 5V             |
| Output | LCD Pin 3 (V0) |
| GND    | GND            |

  ![Circuit](../images/week07/Poten.jpg)

\- RGB LED:

| Pin | Connection                 |
| --- | -------------------------- |
| G   | Resistor >> Arduino Pin 11 |
| R   | Resistor >> Arduino Pin 10 |
| GND | GND                        |
| B   | Resistor >> Arduino Pin 9  |

  ![Circuit](../images/week07/RGB.jpg)

**Physical circuit:**

  ![Circuit](../images/week07/Circuit.jpg)

## Code

- First I made a function that contains a greetings message to be shown on the LCD at the beginning, then I scroll it with the command `lcd.scrollDisplayLeft();`:
   
```C
void Greeting()
{
  // Print first message on LCD
  lcd.setCursor(0,0);
  lcd.print("Fundamentals2020");
  lcd.setCursor(0,1);
  lcd.print(" FabLab - KaLi ");
  RGB_color(0, 0, 255); // Blue
  delay(2500); // wait 2.5 segs

  // Scroll message to the left
  for (int positionCounter = 0; positionCounter < 16; positionCounter++)
  {
    lcd.scrollDisplayLeft();
    delay(150);
  }
  delay(1000); // wait 1 seg
}
```

- I think that the most interesting of this code id the RGB LED part, then of it the Green, Red and Blue colors are controlled independently:

```C
int Glight = 11;
int Rlight= 10;
int Blight = 9; 

void setup()
{
  pinMode(Rlight, OUTPUT);
  pinMode(Glight, OUTPUT);
  pinMode(Blight, OUTPUT);
}
```

- In the loop I get the readings from the Ultrasonic sensor, then it calls a function named `Result(d)`:

```C
void loop()
{
  [Getting readings from Ultrasonic sensor]

  Result(d);
}
```

- The function `Result(d)` changes the color according to the distance that the ultrasonic sensor detects; it does it by calling another function named `RGB_color(red_light_value, green_light_value, blue_light_value);` and giving it the RGB values needed for each color:

```C
void Result(long d)
{
  if (d<10)
  {
    // Start blinking in red
    RGB_color(255, 0, 0); // High red
    delay(500);
    RGB_color(64, 0, 0); // Low red
    delay(500);
  }
  else if (d>15)
  {
    RGB_color(0, 255, 0); // Green
    delay(100);
  }
  else
  {
    RGB_color(255, 200, 0); // Yellow
    delay(100);
  }
}
```

- Finally, the function `RGB_color(red_light_value, green_light_value, blue_light_value);` writes on every RGB pin the received values:

```C
void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
{
  analogWrite(Rlight, red_light_value);
  analogWrite(Glight, green_light_value);
  analogWrite(Blight, blue_light_value);
}
```

**Full code:**

```C
#include <LiquidCrystal.h>
LiquidCrystal lcd(13, 12, 5, 4, 3,2);

// LED_Connection [G R GND B]
int Glight = 11;
int Rlight= 10;
int Blight = 9;

const int Trigger = 7;
const int Echo = 6;

void setup() {
  // LCD setup
  lcd.begin(16, 2);

  // Ultrasonic Sensor setup
  pinMode(Trigger, OUTPUT);
  pinMode(Echo, INPUT);
  digitalWrite(Trigger, LOW);

  // RGB setup
  pinMode(Rlight, OUTPUT);
  pinMode(Glight, OUTPUT);
  pinMode(Blight, OUTPUT);

  Greeting();
}

void loop()
{
  long t; //echo width
  long d; //distance in cm

  // Send 10us pulses to the Trigger
  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(Trigger, LOW);

  t = pulseIn(Echo, HIGH); //get the pulse width
  d = t/59; //get distance in cm

  Result(d);
}

void Greeting()
{
  // Print first message on LCD
  lcd.setCursor(0,0);
  lcd.print("Fundamentals2020");
  lcd.setCursor(0,1);
  lcd.print(" FabLab - KaLi ");
  RGB_color(0, 0, 255); // Blue
  delay(2500); // wait 2.5 segs

  // Scroll message to the left
  for (int positionCounter = 0; positionCounter < 16; positionCounter++)
  {
    lcd.scrollDisplayLeft();
    delay(150);
  }
  delay(1000); // wait 1 seg
}

void Result(long d)
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Distance: " + String(d) + " cm");
  lcd.setCursor(0,1);

  if (d<10)
  {
    lcd.print("    Danger!");
    // Start blinking in red
    RGB_color(255, 0, 0); // High red
    delay(500);
    RGB_color(64, 0, 0); // Low red
    delay(500);
  }
  else if (d>15)
  {
    lcd.print("   Safe area");
    RGB_color(0, 255, 0); // Green
    delay(100);
  }
  else
  {
    lcd.print("   Limit area");
    RGB_color(255, 200, 0); // Yellow
    delay(100);
  }
}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  analogWrite(Rlight, red_light_value);
  analogWrite(Glight, green_light_value);
  analogWrite(Blight, blue_light_value);
}
```

## Demonstration

<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
  <iframe src="https://www.youtube.com/embed/NJSDAsy9Yvg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
</div>

## Download files

\- Code: [SpeedPosAlt.ino](../images/week07/SafetyZone/SafetyZone.ino)  
\- LCD datasheet: [LCM-S01602DTR/M.pdf](../images/week07/LCM-S01602DTRM.pdf)  
\- HC-SR04 example code: [HC-SR04_Example.ino](../images/week07/HC-SR04_ExampleCode/HC-SR04_ExampleCode.ino)  
