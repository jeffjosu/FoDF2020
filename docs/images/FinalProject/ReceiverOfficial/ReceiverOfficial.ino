//Libraries for Communication part (nRF24)
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); //Create radio object with and CE & CSN pins
const byte address[6] = "00111"; //Receiver Communication address

//Libraries for Motor shield
#include <Wire.h>
#include <Adafruit_MotorShield.h>

//Create Motor shield object with the default I2C address 
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

//Declare motors (M1, M2 and M3) on their respective ports.
Adafruit_DCMotor *Motor1 = AFMS.getMotor(1);
Adafruit_DCMotor *Motor2 = AFMS.getMotor(2);
Adafruit_DCMotor *Motor3 = AFMS.getMotor(3);

const double AngWheels[3] = {0, 2.094, 4.189}; //Angle in rads of each wheel with respect to Y-axis of the robot (correspond to 0º, 120º and 240º)

//Library for math functions
#include <math.h> 

void setup() {
  Serial.begin(9600); //Begin Serial communication at 9600 bauds

  radio.begin(); //Initialize radio object
  radio.setPALevel(RF24_PA_HIGH); //Set radio range
  radio.openReadingPipe(0, address); //Set address to transmitter
  radio.startListening(); //Set module as receiver

  AFMS.begin(); //Initialize Motor shield ports
}

void loop() {
  int Values[3];
  
  if (radio.available()) {  
    radio.read(&Values, sizeof(Values)); //Read array of Values from Transmitter
        
      if (Values[2]<=-5 || Values[2]>=5) //Detect if the Joystick_2 is activated
      { int speed = map(Values[2],-100,100,-60,60); //Read the percentage values and convert them to speed values for the motors
        
        //Give direction of rotation to each wheel, depending on direction we push the Joystick
        if (Values[2]<-4) 
        { Motor1->run(FORWARD);
          Motor2->run(FORWARD);
          Motor3->run(FORWARD);
          Serial.println("CCW rotation");}
        if (Values[2]>4)
        { Motor1->run(BACKWARD);
          Motor2->run(BACKWARD);
          Motor3->run(BACKWARD);
          Serial.println("CC rotation");}
          
        //Write speed in each motor
        Motor1->setSpeed(speed);
        Motor2->setSpeed(speed);
        Motor3->setSpeed(speed);
        } else if ((Values[0]<=-4 || Values[0]>=4) || (Values[1]<=-4 || Values[1]>=4))
        {
            //Calculate angle of movement of robot (and Read the percentaje values and convert them to speed values for the motors)
            double Vx=map(Values[1],-100,100,-230,230);
            double Vy=map(Values[0],-100,100,-230,230);
            double theta = atan2(Vx, Vy);

            //Calculate speed for every wheel, using the formula holonomic drive for 3W omnidirectional robots:
            double SpeedM1 = ((cos(theta)*cos(AngWheels[0]) - sin(theta)*sin(theta + AngWheels[0]))*Vx) - ((sin(theta)*cos(theta + AngWheels[0]) + cos(theta)*sin(theta + AngWheels[0]))*Vy);
            double SpeedM2 = ((cos(theta)*cos(AngWheels[1]) - sin(theta)*sin(theta + AngWheels[1]))*Vx) - ((sin(theta)*cos(theta + AngWheels[1]) + cos(theta)*sin(theta + AngWheels[1]))*Vy);
            double SpeedM3 = ((cos(theta)*cos(AngWheels[2]) - sin(theta)*sin(theta + AngWheels[2]))*Vx) - ((sin(theta)*cos(theta + AngWheels[2]) + cos(theta)*sin(theta + AngWheels[2]))*Vy);

            //Give direction of rotation to each wheel, depending on the signal of their speed
            if (SpeedM1<0){ Motor1->run(BACKWARD);}
            else if (SpeedM1>0){ Motor1->run(FORWARD);}
            if (SpeedM2<0){ Motor2->run(BACKWARD);}
            else if (SpeedM2>0){ Motor2->run(FORWARD);}
            if (SpeedM3<0){ Motor3->run(BACKWARD);}
            else if (SpeedM3>0){ Motor3->run(FORWARD);}

            //Write speed in each motor
            Motor1->setSpeed(SpeedM1);
            Motor2->setSpeed(SpeedM2);
            Motor3->setSpeed(SpeedM3);

            if (Values[1]<-4) {Serial.println("Moving to the Left");}
            if (Values[1]>4) {Serial.println("Moving to the Right");}
            if (Values[0]<-4) {Serial.println("Moving Backward");}
            if (Values[0]>4) {Serial.println("Moving Forward");}
            Serial.print("M1: ");
            Serial.println(SpeedM1);
            Serial.print("M2: ");
            Serial.println(SpeedM2);
            Serial.print("M3: ");
            Serial.println(SpeedM3);
            Serial.println("");
        } else { //Deactivate motors
          Motor1->setSpeed(0);
          Motor2->setSpeed(0);
          Motor3->setSpeed(0);}     
   }
}
