const int led = LED_BUILTIN; //the build-in LED on the Board
const int Button0 = 2; //Button in pin 2 -> number 0
const int Button1 = 3; //Button in pin 3 -> number 1

void setup() 
{
  //Declare pinModes (inputs and outputs)
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
  pinMode(Button0, INPUT);
  pinMode(Button1, INPUT);

  //Initialize Serial comunication
  Serial.begin (9600);
}

void loop() 
{
  char message[5];
  int n=0;
  Serial.print("Binario: ");
    while (n<5)
    
    {
      if(digitalRead(Button0) == HIGH)
      { 
      Serial.print("0");
      delay(500);
      message[n]='0';
      n++;
      }
      else if(digitalRead(Button1) == HIGH)
        {
        Serial.print("1");
        delay(500);
        message[n]='1';
        n++;
        }
    }
    Convert(message);
}

void Convert (String message){
 
  char bin[6];
  message.toCharArray(bin, 6);
 
  int i = 0;
  int dec = 0;

  while ( bin[i] == '0' || bin[i] == '1' ) {
    if ( bin[i] == '0' )
      dec <<= 1;
    else {
      dec ^= 1;
      dec <<= 1;}
    ++i;  }
 
  dec >>= 1;
 
  Serial.print(" = Decimal: ");
  Serial.println(dec);
}
