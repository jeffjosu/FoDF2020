#include <LiquidCrystal.h>
LiquidCrystal lcd(13, 12, 5, 4, 3,2);

//LED_Connection [G R GND B]
int Glight = 11;
int Rlight= 10;
int Blight = 9;

const int Trigger = 7;
const int Echo = 6;

void setup() {
  //LCD setup
  lcd.begin(16, 2);

  //Ultrasonic Sensor setup
  pinMode(Trigger, OUTPUT);
  pinMode(Echo, INPUT);
  digitalWrite(Trigger, LOW);

  //RGB setup
  pinMode(Rlight, OUTPUT);
  pinMode(Glight, OUTPUT);
  pinMode(Blight, OUTPUT);
  
  Greeting();
}

void loop() 
{
  long t; //echo width
  long d; //distance in cm

  //Send 10us pulses to the Trigger
  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10); 
  digitalWrite(Trigger, LOW);
  
  t = pulseIn(Echo, HIGH); //get the pulse width
  d = t/59; //get distance in cm
  
  Result(d);
}

void Greeting()
{
  // Print first message on LCD
  lcd.setCursor(0,0);
  lcd.print("Fundamentals2020");
  lcd.setCursor(0,1);
  lcd.print(" FabLab - KaLi ");
  RGB_color(0, 0, 255); // Blue
  delay(2500); // wait 2.5 segs
  
  // scroll message to the left
  for (int positionCounter = 0; positionCounter < 16; positionCounter++)   
  {
    lcd.scrollDisplayLeft();
    delay(150);
  }
  delay(1000); // wait 1 seg
}

void Result(long d)
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Distance: " + String(d) + " cm");
  lcd.setCursor(0,1);
  
  if (d<10)
  {
   lcd.print("    Danger!");
   // Blinking red
   RGB_color(255, 0, 0); // Red High
   delay(500);
   RGB_color(64, 0, 0); // Red Low
   delay(500);
  }
  else if (d>15)
  {
    lcd.print("   Safe area");
    RGB_color(0, 255, 0); // Green
    delay(100);
  }
  else
  {
    lcd.print("   Limit area");
    RGB_color(255, 200, 0); // Yellow
    delay(100);
  }
}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  analogWrite(Rlight, red_light_value);
  analogWrite(Glight, green_light_value);
  analogWrite(Blight, blue_light_value);
}
